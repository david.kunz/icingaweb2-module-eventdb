icingaweb2-module-eventdb (1.3.0-7) unstable; urgency=medium

  * Adding Barzilian Portuguese debconf translations from Paulo Henrique de
    Lima Santana (Closes: #1094252).
  * Updating copyright for d/.
  * Updating Standards-Version to 4.7.0.
  * Adding Portuguese debconf translations from Américo Monteiro
    (Closes: #1099385).

 -- David Kunz <david.kunz@dknet.ch>  Thu, 06 Mar 2025 09:33:21 +0100

icingaweb2-module-eventdb (1.3.0-6) unstable; urgency=medium

  * Updating German debconf translations from Hermann-Josef Beckers
    (Closes: #1066921).
  * Updating Spanish debconf translations from Camaleón.
  * Removing unnecessary line 'X-Generator' in d/{es,fr,ge,nl}.po files.
  * Renaming d/po/ge.po to d/po/de.po to correct language code.

 -- David Kunz <david.kunz@dknet.ch>  Wed, 03 Jul 2024 15:45:56 +0200

icingaweb2-module-eventdb (1.3.0-5) unstable; urgency=medium

  * Adding Swedish debconf translations from Martin Bagge (Closes: #1059122).
  * Adding French debconf translations from Jean-Pierre Giraud
    (Closes: #1060628).
  * Correcting debconf question (Closes: #1052274).
  * Unfuzzy d/*.po files because the change is small.
  * Updating copyright year for Icinga Development Team.
  * Updating copyright for 2024.
  * Correcting misspelling in changelog for 1.3.0-{3,4}.

 -- David Kunz <david.kunz@dknet.ch>  Mon, 18 Mar 2024 16:07:06 +0100

icingaweb2-module-eventdb (1.3.0-4) unstable; urgency=medium

  * Configure debconf.
  * Adding upstream metadata.
  * Updating copyright for debian directory.
  * Updating Standards-Version to 4.6.2.
  * Remove duplicate Section in control.
  * Adding Rules-Requires-Root to control.
  * Remove unused doc files.
  * Standardization of the rules for icingaweb2 modules.
  * Standardization of the control for icingaweb2 modules.
  * Updating docs.
  * Adding Spanish debconf translations from Camaleón.
  * Adding Dutch debconf translations from Francs Spiesschaert.
  * Adding German debconf translations from Christoph Brinkhaus.

 -- David Kunz <david.kunz@dknet.ch>  Wed, 23 Aug 2023 15:40:44 +0200

icingaweb2-module-eventdb (1.3.0-3) unstable; urgency=medium

  * Adding watch file.
  * Updating Standards-Version.
  * Updating vcs in control.
  * Updating copyright.
  * Removing .mailmap.

 -- David Kunz <david.kunz@dknet.ch>  Wed, 29 Dec 2021 11:42:06 +0100

icingaweb2-module-eventdb (1.3.0-2) unstable; urgency=medium

  * Updating debhelper.
  * Updating standard version.
  * Fixing typo in path (Closes: #952981).
  * Fixing typo in previous changelog entry.

 -- David Kunz <david.kunz@dknet.ch>  Mon, 22 Jun 2020 07:51:37 +0200

icingaweb2-module-eventdb (1.3.0-1) unstable; urgency=medium

  * Initial release (Closes: #921086).

 -- David Kunz <david.kunz@dknet.ch>  Mon, 12 Aug 2019 15:32:37 +0200
